package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

    @NotNull
    EntityManager getEntityManager();

}
