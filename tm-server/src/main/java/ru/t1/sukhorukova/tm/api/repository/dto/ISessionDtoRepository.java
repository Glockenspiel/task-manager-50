package ru.t1.sukhorukova.tm.api.repository.dto;

import ru.t1.sukhorukova.tm.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IUserOwnerDtoRepository<SessionDTO> {
}
